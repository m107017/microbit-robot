//*******************************************************************************
// ファイル名   ：microbit.js
// 説明         ：micro:bit制御用JavaScript（ライントレース制御）
//*******************************************************************************

let startLineTrace = 0                                  // ライントレース状態
let sensorData = 0                                      // センサデータ
let driverAin1 = 0                                      // DRV8835 AIN1出力値
let driverAin2 = 0                                      // DRV8835 AIN2出力値
let driverBin1 = 0                                      // DRV8835 BIN1出力値
let driverBin2 = 0                                      // DRV8835 BIN2出力値

//*******************************************************************************
// 関数名       ：sensor
// 説明         ：センサデータ取得＆LED点灯
//*******************************************************************************
function sensor() {

    sensorData = 0                                      // センサデータクリア

    if( pins.digitalReadPin(DigitalPin.P16) == 0 )      // ●？？？
    {
        led.plot( 4, 4 )
        sensorData += 8
    }
    else                                                // 〇？？？
    {
        led.unplot( 4, 4 )
    }

    if( pins.digitalReadPin(DigitalPin.P15) == 0 )      // ？●？？
    {
        led.plot( 3, 4 )
        sensorData += 4
    }
    else                                                // ？〇？？
    {
        led.unplot( 3, 4 )
    }

    if( pins.digitalReadPin(DigitalPin.P14) == 0 )      // ？？●？
    {
        led.plot( 2, 4 )
        sensorData += 2
    }
    else                                                // ？？〇？
    {
        led.unplot( 2, 4 )
    }

    if( pins.digitalReadPin(DigitalPin.P13) == 0 )      // ？？？●
    {
        led.plot( 1, 4 )
        sensorData += 1
    }
    else                                                // ？？？〇
    {
        led.unplot( 1, 4 )
    }

}

//*******************************************************************************
// 関数名       ：motorControl
// 説明         ：モータ制御
//*******************************************************************************
function motorControl() {

    if( sensorData == 8 || sensorData == 12 || sensorData == 4 )        // ●〇〇〇、●●〇〇、〇●〇〇（線に対して右にいる？）
    {
        driverAin1 = 1                                                  // 右モータだけ動かす
        driverAin2 = 0
        driverBin1 = 1
        driverBin2 = 1
    }
    else if( sensorData == 6 )                                          // 〇●●〇（線に対して中央にいる？）
    {
        driverAin1 = 1                                                  // 両モータを動かす
        driverAin2 = 0
        driverBin1 = 1
        driverBin2 = 0
    }
    else if( sensorData == 2 || sensorData == 3 || sensorData == 1 )    // 〇〇〇●、〇〇●●、〇〇●〇（線に対して左にいる？）
    {
        driverAin1 = 1                                                  // 左モータだけ動かす
        driverAin2 = 1
        driverBin1 = 1
        driverBin2 = 0
    }
    else
    {
        // それ以外の場合は、前回の値を使用する
    }

    pins.digitalWritePin( DigitalPin.P0, driverAin1 )
    pins.digitalWritePin( DigitalPin.P1, driverAin2 )
    pins.digitalWritePin( DigitalPin.P8, driverBin1 )
    pins.digitalWritePin( DigitalPin.P12, driverBin2 )

}

//*******************************************************************************
// 関数名       ：motorStop
// 説明         ：モータ停止
//*******************************************************************************
function motorStop() {

    pins.digitalWritePin( DigitalPin.P0, 0 )
    pins.digitalWritePin( DigitalPin.P1, 0 )
    pins.digitalWritePin( DigitalPin.P8, 0 )
    pins.digitalWritePin( DigitalPin.P12, 0 )

}

//*******************************************************************************
// メインループ
//*******************************************************************************

basic.forever(function () {

    sensor( )                                           // センサデータ取得＆LED点灯
    if( startLineTrace == 1 )                           // ライントレースする？
    {
        motorControl( )                                 // モータ制御
        basic.pause( 7 )                                // 一定時間待ち
        motorStop( )                                    // モータ停止
        basic.pause( 3 )                                // 一定時間待ち
    }
    else                                                // ライントレースしない？
    {
        motorStop( )                                    // モータ停止
        basic.pause( 10 )                               // 一定時間待ち
    }

})

//*******************************************************************************
// ボタンAが押された時の処理
//*******************************************************************************

input.onButtonPressed(Button.A, function () {
    startLineTrace = 1                                  // ライントレース開始
})

//*******************************************************************************
// ボタンBが押された時の処理
//*******************************************************************************

input.onButtonPressed(Button.B, function () {
    startLineTrace = 0                                  // ライントレース停止
})
